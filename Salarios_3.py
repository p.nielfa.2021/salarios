#Código
class Empleado:  #Define lo que es el empleado
    def __init__(self, n, s):   #__init__ le indica a python como se construye, con una variable n y s
        self.nombre = n
        self.nomina = s

    def calculaimpuestos (self):
        impuestos = self.nomina * 0.3
        return impuestos

    def __str__ (self):   #
        return ("El empleado {nombre} paga {impuestos:.2f}€ en impuestos".format(nombre = self.nombre,impuestos=self.calculaimpuestos())


#Ejemplo de como se usaría
empleadoAna = Empleado("Ana", 30000)
empleadoPepe = Empleado("Pepe", 20000)

total = empleadoPepe.calculaimpuestos () + empleadoAna.calculaimpuestos ()

print (empleadoPepe)
print (empleadoAna)
print ("Los impuestos totales a pagar son {:.2f}€".format(total))      