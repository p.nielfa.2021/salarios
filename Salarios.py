#Datos
nomina_ana = 30000
nomina_pepe = 20000
impuestos_porcentaje = 0.3

#Código
def CalculoImpuestos (nomina,porcentaje_de_impuestos):
    impuestos = nomina * porcentaje_de_impuestos
    return impuestos
def CalculoCosteSalarial (nomina,impuestos):
    coste_salarial = nomina + impuestos
    return coste_salarial

impuestos_ana = int (CalculoImpuestos (nomina_ana, impuestos_porcentaje))
impuestos_pepe = int (CalculoImpuestos (nomina_pepe, impuestos_porcentaje))
coste_salarial_ana = int (CalculoCosteSalarial (nomina_ana, impuestos_ana))
coste_salarial_pepe = int (CalculoCosteSalarial (nomina_pepe, impuestos_pepe))

print ("Los impuestos que tiene que pagar Ana son de", impuestos_ana,"€, y su coste salarial es de", coste_salarial_ana,"€")
print ("Los impuestos que tiene que pagar Pepe son de", impuestos_pepe,"€, y su coste salarial es de", coste_salarial_pepe,"€")
