#Código
class Empleado: #Define lo que es el empleado
    def __init__(self, n, s):   #__init__ le indica a python como se construye, con una variable n y s
        self.nombre = n
        self.nomina = s

    def calculaimpuestos (self):
        impuestos = self.nomina * 0.3
        return impuestos

    def __str__ (self):   #
        return ("El empleado {nombre} paga {impuestos:.2f}€ en impuestos".format(nombre = self.nombre,impuestos=self.calculaimpuestos())


class TestEmpleado(unittest.TestCase):

    def test_construir(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.nomina, 5000)

    def test_impuestos(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.calcula_impuestos(), 1500)
   
    def test_str(self):
        e1 = Empleado("pepe",50000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())

if __name__ == "__main__":
    unittest.main()
     
