import unittest
from empleado import Empleado

class TestEmpleado(unittest.TestCase):

    def test_construir(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.nomina, 5000)

    def test_impuestos(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.calculaimpuestos(), 1500)
   
    def test_str(self):
        e1 = Empleado("Pepe",50000)
        self.assertEqual("El empleado Pepe debe pagar 15000.00", e1.__str__())

if __name__ == "__main__":
    unittest.main()
